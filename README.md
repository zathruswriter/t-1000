[![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)](https://GitHub.com/Naereen/StrapDown.js/graphs/commit-activity)
[![MIT license](https://img.shields.io/badge/license-MIT-brightgreen)](https://choosealicense.com/licenses/mit/)
[![Bitbucket issues](https://img.shields.io/bitbucket/issues-raw/zathruswriter/t-1000)](https://bitbucket.org/zathruswriter/t-1000/issues?status=new&status=open)
[![Bitbucket pull requests](https://img.shields.io/bitbucket/pr-raw/zathruswriter/t-1000)](https://bitbucket.org/zathruswriter/t-1000/src)
[![CirceCI status](https://img.shields.io/circleci/build/bitbucket/zathruswriter/t-1000)](https://bitbucket.org/zathruswriter/t-1000/addon/pipelines/home)
[![Coverage Status](https://coveralls.io/repos/bitbucket/zathruswriter/t-1000/badge.svg?branch=master)](https://coveralls.io/bitbucket/zathruswriter/t-1000?branch=master)
[![Chat on Slack](https://img.shields.io/badge/join%20the%20discussion-Slack-yellow)](https://join.slack.com/t/t-1000workspace/shared_invite/enQtNzc4NzcyNzQyNzI3LWU4NDdhNjc1OGI5YjRkYjgwYWE2MzJmMGZlMmY1MWJmZWJlNTQ5NTFhMTU1NDI1MTg2ZWZmMDE4MWJiMjMzOWQ)
[![Twitter URL](https://img.shields.io/twitter/url/http/shields.io.svg?style=social)](https://twitter.com/cata7007)

T-1000 Alpha, a Travian S3 Clone in PHP & MySQL / MariaDB
======
**Alpha:** at this early stage, this project is intended for 
pioneers, PHP developers and early adopters. It currently lacks 
many features originally found in Travian S3. A new set of features 
is being added all the time and the game is slowly being brought 
to life.

**Roadmap:**
TBC

**Quick links:**

* [Download and Updates](https://bitbucket.org/zathruswriter/t-1000/downloads/) &raquo;&raquo; https://bitbucket.org/zathruswriter/t-1000/downloads
* [Wiki](https://bitbucket.org/zathruswriter/t-1000/wiki/)
* [Game Mechanics](http://travian.wikia.com/wiki/Travian_Wiki)
* [Donate to T-1000](https://www.paypal.me/martinambrus)

**Minimum requirements:**

* [PHP](http://php.net/) 5.6.40+
* [MySQL Community Server](https://dev.mysql.com/downloads/mysql/) 5.6+
  * or alternatively, [MariaDB](https://downloads.mariadb.org/) 10.1+

**Support & Bug reports**

We are usually available to chat at our [Slack Room](https://join.slack.com/t/t-1000workspace/shared_invite/enQtNzc4NzcyNzQyNzI3LWU4NDdhNjc1OGI5YjRkYjgwYWE2MzJmMGZlMmY1MWJmZWJlNTQ5NTFhMTU1NDI1MTg2ZWZmMDE4MWJiMjMzOWQ), if you like to ask 
any questions or just talk to the developers about how great their day is today :) As for bug reports, please use 
the [Issues tab](https://bitbucket.org/zathruswriter/t-1000/issues?status=new&status=open) and create new issue, whether it's an error in game 
or you have a feature request to be included in the play.